package sohail.aziz.myflickrapp.auth;

import java.io.IOException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sohail.aziz.myflickrapp.FlickrHelper;
import sohail.aziz.myflickrapp.MainActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.gmail.yuyang226.flickr.Flickr;
import com.gmail.yuyang226.flickr.FlickrException;
import com.gmail.yuyang226.flickr.auth.Permission;
import com.gmail.yuyang226.flickr.oauth.OAuthToken;

public class oAuthTask extends AsyncTask<Void, Void, String> {

	Context mContext;

	private static final Logger logger = LoggerFactory
			.getLogger(oAuthTask.class);
	private static final Uri OAUTH_CALLBACK_URI = Uri
			.parse(MainActivity.CALLBACK_SCHEME + "://oauth"); //$NON-NLS-1$

	public oAuthTask(Context mContext) {
		super();
		this.mContext = mContext;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		if (result != null && !result.startsWith("error")) { //$NON-NLS-1$
			Log.d("sohail", "starting activity for url");
			mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri
					.parse(result)));
		} else {
			Toast.makeText(mContext, result, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	protected String doInBackground(Void... params) {
		// TODO Auto-generated method stub
		String result = null;
		try {

			/*
			 * Flickr f = new Flickr(API_KEY, API_SECRET); // get a request
			 * token from Flickr
			 * 
			 * OAuthToken oauthToken = f.getOAuthInterface().getRequestToken(
			 * OAUTH_CALLBACK_URI.toString());
			 */

			// you should save the request token and token secret to a
			// preference
			// store for later use.

			Flickr f = FlickrHelper.getInstance().getFlickr();
			OAuthToken oauthToken = f.getOAuthInterface().getRequestToken(
					OAUTH_CALLBACK_URI.toString());

			saveTokenSecrent(oauthToken.getOauthTokenSecret());

			// build the Authentication URL with the required permission
			// saveTokenSecrent(oauthToken.getOauthTokenSecret());

			URL oauthUrl = f.getOAuthInterface().buildAuthenticationUrl(
					Permission.READ, oauthToken);
			// return oauthUrl.toString();

			result = oauthUrl.toString();
			Log.d("sohail", "oauthUrl=" + result);

			// redirect user to the genreated URL.
			// redirect(oauthUrl);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FlickrException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;

	}

	/**
	 * Saves the oauth token secrent.
	 * 
	 * @param tokenSecret
	 */
	private void saveTokenSecrent(String tokenSecret) {
		logger.debug("request token: " + tokenSecret); //$NON-NLS-1$
		MainActivity act = (MainActivity) mContext;
		act.saveOAuthToken(null, null, null, tokenSecret);
		logger.debug("oauth token secret saved: {}", tokenSecret); //$NON-NLS-1$
	}

}
