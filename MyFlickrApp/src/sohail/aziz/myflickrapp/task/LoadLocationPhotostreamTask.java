package sohail.aziz.myflickrapp.task;

/**
 * 
 */

import java.util.HashSet;
import java.util.Set;

import sohail.aziz.myflickrapp.FlickrHelper;
import sohail.aziz.myflickrapp.view.HomeActivity;
import sohail.aziz.myflickrapp.view.ImageAdapter;
import android.app.Activity;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Gallery;

import com.gmail.yuyang226.flickr.Flickr;
import com.gmail.yuyang226.flickr.photos.Extras;
import com.gmail.yuyang226.flickr.photos.PhotoList;
import com.gmail.yuyang226.flickr.photos.SearchParameters;
import com.gmail.yuyang226.flickr.places.Place;
import com.gmail.yuyang226.flickr.places.PlacesList;

public class LoadLocationPhotostreamTask extends
		AsyncTask<Location, Void, PhotoList> {

	/**
	 * 
	 */
	private Gallery galleryView;
	private Activity activity;

	/**
	 * @param flickrjAndroidSampleActivity
	 */
	public LoadLocationPhotostreamTask(Activity activity, Gallery gallery) {
		this.activity = activity;
		this.galleryView = gallery;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected PhotoList doInBackground(Location... arg0) {

		// sohail

		PhotoList photos;
		Location loc = arg0[0];

		Flickr f = FlickrHelper.getInstance().getFlickr();

		// url small square,large,date,tags and total views.
		Set<String> extras = new HashSet<String>();
		extras.addAll(Extras.ALL_EXTRAS);

		try {

			//***First Approach***: GetPhoto List using GeoInterface
			// but this function doesnot return anything, no effect of accuracy
			/*
			GeoData location = new GeoData();

			location.setAccuracy(Flickr.ACCURACY_CITY);
			location.setLatitude((float) loc.getLatitude());
			location.setLongitude((float) loc.getLongitude());

			
			photos=f.getGeoInterface().photosForLocation(location, extras, 20, 1);

		
			*/
			
			/*
			 * ****Second approach*****: 
			 * 1-get the place_Id by lat long 
			 * 2-use that place id to get photos using photosInterface.search(place_id)
			 */
			PlacesList pl = f.getPlacesInterface().findByLatLon(
					loc.getLatitude(), loc.getLongitude(), 16);
			
			//findbyLatLon returns exception, bug reported to developer.

			Place p = pl.get(0);
			SearchParameters sp = new SearchParameters();
			sp.setPlaceId(p.getPlaceId());
			photos=f.getPhotosInterface().search(sp, 20, 1);

			
			return photos;
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(PhotoList result) {
		if (result != null) {
			Log.e("sohail", "photolist size=" + result.size());
			// set list ref to homeactivity
			HomeActivity activity = (HomeActivity) this.activity;
			activity.setPhotoList(result);

			ImageAdapter adapter = new ImageAdapter(this.activity, result);
			this.galleryView.setAdapter(adapter);

		} else {
			Log.e("sohail", "null returned in PhotoList");
		}

	}

}