/**
 * 
 */
package sohail.aziz.myflickrapp.task;

import java.util.HashSet;
import java.util.Set;

import sohail.aziz.myflickrapp.FlickrHelper;
import sohail.aziz.myflickrapp.view.HomeActivity;
import sohail.aziz.myflickrapp.view.ImageAdapter;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Gallery;

import com.gmail.yuyang226.flickr.Flickr;
import com.gmail.yuyang226.flickr.photos.Extras;
import com.gmail.yuyang226.flickr.photos.PhotoList;

public class LoadPhotostreamTask extends AsyncTask<Void, Void, PhotoList> {

	/**
	 * 
	 */
	private Gallery galleryView;
	private Activity activity;
	ProgressDialog progressDialog;

	/**
	 * @param flickrjAndroidSampleActivity
	 */
	public LoadPhotostreamTask(Activity activity, Gallery gallery) {
		this.activity = activity;
		this.galleryView = gallery;

	}

	@Override
	protected PhotoList doInBackground(Void... params) {

		Flickr mf = FlickrHelper.getInstance().getFlickr();

		Set<String> extras = new HashSet<String>();

		extras.addAll(Extras.ALL_EXTRAS);

		try {

			return mf.getInterestingnessInterface().getList("", extras, 20, 1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		progressDialog = new ProgressDialog(activity);
		progressDialog.setCancelable(false);
		progressDialog.setTitle("Please wait");
		progressDialog.setMessage("Fetching photos. . .");
		progressDialog.setProgress(0);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.show();

	}

	@Override
	protected void onPostExecute(PhotoList result) {

		if (progressDialog.isShowing()) {

			progressDialog.dismiss();
		}
		if (result != null) {
			Log.e("sohail", "photolist size=" + result.size());
			// set list ref to homeactivity
			HomeActivity activity = (HomeActivity) this.activity;
			activity.setPhotoList(result);

			ImageAdapter adapter = new ImageAdapter(this.activity, result);
			this.galleryView.setAdapter(adapter);

		} else {
			Log.e("sohail", "null returned in PhotoList");
		}

	}

}