package sohail.aziz.myflickrapp.view;

import java.util.Collection;

import sohail.aziz.myflickrapp.R;
import sohail.aziz.myflickrapp.task.LoadPhotostreamTask;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;

import com.gmail.yuyang226.flickr.oauth.OAuth;
import com.gmail.yuyang226.flickr.photos.Photo;
import com.gmail.yuyang226.flickr.tags.Tag;

public class GalleryFragment extends Fragment implements OnItemClickListener {

	private Gallery glImageGallery;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);

		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_home,
				container, false);
		glImageGallery = (Gallery) root.findViewById(R.id.glImageGallery);
		glImageGallery.setOnItemClickListener(this);

		// so it can be used by location listener
		((HomeActivity) getActivity()).setGalleryRef(glImageGallery);

		new LoadPhotostreamTask(getActivity(), glImageGallery).execute();

		return root;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

		HomeActivity activity = (HomeActivity) getActivity();

		Photo selectedPhoto = activity.getPhoto(position);

		if (selectedPhoto != null) {

			String largeUrl = selectedPhoto.getLargeUrl();
			Log.e("sohail", "largeurl=" + largeUrl);
			Collection<Tag> tags = selectedPhoto.getTags();
			int views = selectedPhoto.getViews();
			Log.e("sohail", "views=" + views);

			String title = selectedPhoto.getTitle();
			Log.e("sohail", "title=" + title);

			String description = selectedPhoto.getDescription();
			Log.e("sohail", "description=" + description);

			FragmentTransaction ft = getFragmentManager().beginTransaction();

			LargePhotoFragment f = (LargePhotoFragment) LargePhotoFragment
					.newInstance(position);

			ft.add(android.R.id.content, f);
			ft.hide(GalleryFragment.this);
			ft.show(f);
			ft.addToBackStack(null);
			ft.commit();

		}
	}

}
