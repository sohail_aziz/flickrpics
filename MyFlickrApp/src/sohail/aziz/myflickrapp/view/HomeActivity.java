package sohail.aziz.myflickrapp.view;

import sohail.aziz.myflickrapp.task.LoadLocationPhotostreamTask;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Gallery;

import com.gmail.yuyang226.flickr.photos.Photo;
import com.gmail.yuyang226.flickr.photos.PhotoList;

public class HomeActivity extends Activity {

	private PhotoList photosRefs;
	private Gallery galleryRef;
	private Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		// setContentView(R.layout.activity_home);

		LocationManager lm = (LocationManager) getSystemService(this.LOCATION_SERVICE);
		LocationListener ll = new MyLocationListener();
		// get location updates for 1km change
		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, ll);

		activity = this;
		showGalleryFragment();

	}

	private void showGalleryFragment() {
		// TODO Auto-generated method stub
		FragmentTransaction ft = getFragmentManager().beginTransaction();
		GalleryFragment f = new GalleryFragment();
		ft.add(android.R.id.content, f);
		ft.commit();

	}

	/**
	 * @return the galleryRef
	 */
	public Gallery getGalleryRef() {
		return galleryRef;
	}

	/**
	 * @param galleryRef
	 *            the galleryRef to set
	 */
	public void setGalleryRef(Gallery galleryRef) {
		this.galleryRef = galleryRef;
	}

	public void setPhotoList(PhotoList list) {
		if (list != null) {
			photosRefs = list;
		}
	}

	public Photo getPhoto(int position) {

		if (position >= 0)
			return photosRefs.get(position);
		else
			return null;
	}

	private class MyLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub

			Log.e("sohail",
					"Location changed! new location lat="
							+ location.getLatitude() + " long="
							+ location.getLongitude());

			// start locationPhotoStreamTask

			new LoadLocationPhotostreamTask(activity, getGalleryRef())
					.execute(location);

		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

	}

}
