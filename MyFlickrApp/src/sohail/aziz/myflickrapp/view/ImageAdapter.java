package sohail.aziz.myflickrapp.view;

import sohail.aziz.myflickrapp.R;
import sohail.aziz.myflickrapp.task.ImageDownloadTask;
import sohail.aziz.myflickrapp.utils.ImageUtils.DownloadedDrawable;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.gmail.yuyang226.flickr.photos.Photo;
import com.gmail.yuyang226.flickr.photos.PhotoList;

public class ImageAdapter extends BaseAdapter {

	private Context mContext;
	private PhotoList list;
	private LayoutInflater inflator;

	public ImageAdapter(Context ctx, PhotoList photos) {
		mContext = ctx;
		list = photos;
		inflator = LayoutInflater.from(ctx);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		viewHolder holder;

		if (convertView == null) {

			holder = new viewHolder();

			ImageView image = new ImageView(mContext);
			image.setPadding(3, 3, 3, 3);
			holder.ivImage = image;
			convertView = image;
			convertView.setTag(holder);
		} else {
			holder = (viewHolder) convertView.getTag();
		}

		Photo photo = list.get(position);

		// pass to imageView to update, when downloaded
		ImageDownloadTask task = new ImageDownloadTask(holder.ivImage);

		Drawable drawable = new DownloadedDrawable(task);

		holder.ivImage.setImageDrawable(drawable);
		holder.ivImage.setScaleType(ImageView.ScaleType.FIT_XY);

		task.execute(photo.getSmallSquareUrl());
		//medium too large for internal cache
		//	task.execute(photo.getMediumUrl());

		return convertView;
	}

	private static class viewHolder {
		ImageView ivImage;
	}

}
