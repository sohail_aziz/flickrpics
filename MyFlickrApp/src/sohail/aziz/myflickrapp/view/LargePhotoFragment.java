package sohail.aziz.myflickrapp.view;

import com.gmail.yuyang226.flickr.photos.Photo;

import sohail.aziz.myflickrapp.R;
import sohail.aziz.myflickrapp.task.ImageDownloadTask;
import sohail.aziz.myflickrapp.utils.ImageUtils.DownloadedDrawable;
import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class LargePhotoFragment extends Fragment {

	private LargePhotoFragment() {

	}

	public static Fragment newInstance(int position) {

		LargePhotoFragment f = new LargePhotoFragment();

		Bundle b = new Bundle();
		b.putInt("index", position);

		f.setArguments(b);
		return f;
	}

	TextView tvTitle, tvViews;
	ImageView ivPhoto;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);

		ViewGroup root = (ViewGroup) inflater.inflate(
				R.layout.large_photo_fragment, container, false);

		tvTitle = (TextView) root.findViewById(R.id.tvTitle);
		tvViews = (TextView) root.findViewById(R.id.tvViews);
		ivPhoto = (ImageView) root.findViewById(R.id.ivLargePhoto);

		HomeActivity activity = (HomeActivity) getActivity();

		int position = getArguments().getInt("index");
		Photo p = activity.getPhoto(position);

		String title = p.getTitle();
		if (title != null) {
			tvTitle.setText(title);
		}

		int views = p.getViews();
		if (views > 0) {

			tvViews.setText("Views:" + String.valueOf(views));
		}

		// load large photo
		ImageDownloadTask task = new ImageDownloadTask(ivPhoto);

		Drawable drawable = new DownloadedDrawable(task);
		ivPhoto.setImageDrawable(drawable);
		ivPhoto.setScaleType(ImageView.ScaleType.FIT_XY);
		task.execute(p.getLargeUrl());

		return root;

	}

}
